package com.industry.soft.testtask.model;

import io.realm.RealmObject;

/**
 * Model for notice the weather day to diary.
 */
public class Day extends RealmObject {

    private String mTitle;
    private String mDate;
    private String mLocation;
    private String mDegree;
    private String mImagePath;
    private String mDescription;

    public Day() {
    }

    public Day(String title, String date, String location, String degree, String imagePath, String description) {
        mTitle = title;
        mDate = date;
        mLocation = location;
        mDegree = degree;
        mImagePath = imagePath;
        mDescription = description;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String getDegree() {
        return mDegree;
    }

    public void setDegree(String degree) {
        mDegree = degree;
    }

    public String getImagePath() {
        return mImagePath;
    }

    public void setImagePath(String path) {
        mImagePath = path;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    @Override
    public String toString() {
        return "      title: " + mTitle + '\n' +
                "       date: " + mDate + '\n' +
                "   location: " + mLocation + '\n' +
                "     degree: " + mDegree + '\n' +
                " image path: " + mImagePath + '\n' +
                "description: " + mDescription;
    }
}
