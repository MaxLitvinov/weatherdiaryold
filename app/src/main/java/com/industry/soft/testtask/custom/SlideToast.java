package com.industry.soft.testtask.custom;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ru.terrakok.cicerone.commands.Command;

public class SlideToast extends RelativeLayout implements Command {

    private static final long ANIMATION_DURATION = 300; // Milliseconds.
    private static final long ANIMATION_START_OFFSET = 2000; // Milliseconds.

    private TextView mText;
    private Animation mAnimation;

    public SlideToast(Context context) {
        super(context);
        init(context);
    }

    public SlideToast(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SlideToast(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SlideToast(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        mText = new TextView(context);
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(android.R.attr.actionBarSize, typedValue, true);
        int height = TypedValue.complexToDimensionPixelSize(typedValue.data, getResources().getDisplayMetrics());
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, height);
        textParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        mText.setLayoutParams(textParams);
        mText.setTextColor(Color.WHITE);
        mText.setGravity(Gravity.CENTER);

        this.setLayoutParams(new LayoutParams(
                LayoutParams.MATCH_PARENT,
                getPaddingTop() + getPaddingBottom() + mText.getHeight()));
        this.addView(mText);
    }

    public SlideToast makeToast(String message, @ColorRes int colorRes) {
        setBackgroundColor(ContextCompat.getColor(getContext(), colorRes));
        mText.setText(message);
        return this;
    }

    public void show() {
        if (mAnimation == null) {
            slideDown();
        }
    }

    private void slideDown() {
        mAnimation = new TranslateAnimation(0, 0, -getHeight(), 0);
        mAnimation.setDuration(ANIMATION_DURATION);
        mAnimation.setFillAfter(true);
        mAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                slideUp();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        startAnimation(mAnimation);
    }

    private void slideUp() {
        mAnimation = new TranslateAnimation(0, 0, 0, -getHeight() * 3);
        mAnimation.setDuration(ANIMATION_DURATION);
        mAnimation.setStartOffset(ANIMATION_START_OFFSET);
        mAnimation.setFillAfter(true);
        mAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mAnimation = null;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        startAnimation(mAnimation);
    }
}