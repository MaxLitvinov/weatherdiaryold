package com.industry.soft.testtask.cicerone;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.industry.soft.testtask.R;
import com.industry.soft.testtask.fragment.add.AddFragment;
import com.industry.soft.testtask.fragment.list.ListFragment;

import ru.terrakok.cicerone.android.SupportFragmentNavigator;
import ru.terrakok.cicerone.commands.Command;

public class AppNavigator extends SupportFragmentNavigator {

    private Activity mActivity;

    /**
     * Creates SupportFragmentNavigator.
     *
     * @param fragmentManager support fragment manager
     * @param containerId     id of the fragments container layout
     */
    public AppNavigator(Activity activity, FragmentManager fragmentManager, int containerId) {
        super(fragmentManager, containerId);
        mActivity = activity;
    }

    @Override
    public Fragment createFragment(String screenKey, Object data) {
        switch (screenKey) {
            case Screens.ADD_FRAGMENT:
                return AddFragment.newInstance();
            case Screens.LIST_FRAGMENT:
                return ListFragment.newInstance();
            default:
                throw new RuntimeException("Unknown screen key!");
        }
    }

    public Fragment getFragment(int position) {
        switch (position) {
            case 0:
                return AddFragment.newInstance();
            case 1:
                return ListFragment.newInstance();
            default:
                throw new RuntimeException("Unknown fragment position!");
        }
    }

    @Override
    protected void showSystemMessage(String message) {
    }

    @Override
    protected void exit() {
        mActivity.finish();
    }

    @Override
    protected void setupFragmentTransactionAnimation(Command command, Fragment currentFragment, Fragment nextFragment, FragmentTransaction fragmentTransaction) {
        super.setupFragmentTransactionAnimation(command, currentFragment, nextFragment, fragmentTransaction);
        if (currentFragment instanceof AddFragment) {
            rightToLeft(fragmentTransaction);
        } else {
            leftToRight(fragmentTransaction);
        }
    }

    private void rightToLeft(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.setCustomAnimations(R.anim.fragment_enter_from_right, R.anim.fragment_exit_to_left);
    }

    private void leftToRight(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.setCustomAnimations(R.anim.fragment_enter_from_left, R.anim.fragment_exit_to_right);
    }
}
