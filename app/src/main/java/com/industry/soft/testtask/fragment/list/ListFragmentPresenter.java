package com.industry.soft.testtask.fragment.list;

import android.os.Handler;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.industry.soft.testtask.cicerone.AppRouter;
import com.industry.soft.testtask.cicerone.Screens;
import com.industry.soft.testtask.fragment.list.adapter.DayAdapter;
import com.industry.soft.testtask.realm.RealmManager;

@InjectViewState
public class ListFragmentPresenter extends MvpPresenter<ListFragmentView> {

    private AppRouter mRouter;
    private RealmManager mRealmManager;

    public ListFragmentPresenter(AppRouter router, RealmManager realmManager) {
        mRouter = router;
        mRealmManager = realmManager;
    }

    public void loadDays(final DayAdapter adapter) {
        mRealmManager.load(adapter);
    }

    public void loadMoreDays(final DayAdapter adapter) {
        adapter.getDays().add(null);
        adapter.notifyItemInserted(adapter.getDays().size() - 1);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                adapter.getDays().remove(adapter.getDays().size() - 1);
                int start = adapter.getDays().size();
                int end = start + 20;

                // TODO: Load days.
                mRealmManager.load(adapter, start, end);
            }
        }, 500);
        adapter.setLoaded();
    }

    public void onBackPressed() {
        mRouter.backTo(Screens.ADD_FRAGMENT);
    }
}
