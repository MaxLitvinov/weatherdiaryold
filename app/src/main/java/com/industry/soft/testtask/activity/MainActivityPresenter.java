package com.industry.soft.testtask.activity;

import android.support.v4.app.FragmentManager;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.industry.soft.testtask.cicerone.Screens;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class MainActivityPresenter extends MvpPresenter<MainActivityView> {

    private Router mRouter;
    private FragmentManager mFragmentManager;

    public MainActivityPresenter(Router router, FragmentManager manager) {
        mRouter = router;
        mFragmentManager = manager;
    }

    public void loadStartFragment() {
        mRouter.newScreenChain(Screens.ADD_FRAGMENT);
    }

    public void showAddFragment() {
        if (!isActive(Screens.ADD_FRAGMENT)) {
            mRouter.navigateTo(Screens.ADD_FRAGMENT);
        }
    }

    public void showListFragment() {
        if (!isActive(Screens.LIST_FRAGMENT)) {
            mRouter.navigateTo(Screens.LIST_FRAGMENT);
        }
    }

    /**
     *  @param screenKey Key of the {@link Screens} class should be named like fragment name.
     * @return True if fragment is active now.
     */
    private boolean isActive(String screenKey) {
        return screenKey.equals(getCurrentScreen());
    }

    private String getCurrentScreen() {
        if (mFragmentManager.getBackStackEntryCount() > 0) {
            return mFragmentManager.getBackStackEntryAt(mFragmentManager.getBackStackEntryCount() - 1).getName();
        } else {
            return "";
        }
    }
}
