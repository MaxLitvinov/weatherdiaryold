package com.industry.soft.testtask.fragment.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.industry.soft.testtask.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProgressHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.pb_adapter_progress)
    protected ProgressBar mProgressBar;

    public ProgressHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind() {
        mProgressBar.setIndeterminate(true);
    }
}
