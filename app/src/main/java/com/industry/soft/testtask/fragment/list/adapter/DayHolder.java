package com.industry.soft.testtask.fragment.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.industry.soft.testtask.R;
import com.industry.soft.testtask.custom.RoundedImageView;
import com.industry.soft.testtask.model.Day;
import com.industry.soft.testtask.utils.LoadImageFromFile;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DayHolder extends RecyclerView.ViewHolder {

    private final Context mContext;

    @BindView(R.id.iv_adapter_background)
    ImageView mBackground;
    @BindView(R.id.iv_image)
    RoundedImageView mImage;
    @BindView(R.id.tv_title)
    TextView mTitle;
    @BindView(R.id.tv_description)
    TextView mDescription;

    public DayHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mContext = itemView.getContext();
    }

    public void bind(Day day) {
        mTitle.setText(day.getTitle());
        if (multiple(getAdapterPosition())) {
            mBackground.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.adapter_background_1));
        } else {
            mBackground.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.adapter_background_2));
        }
        mDescription.setText(day.getDescription());
        new LoadImageFromFile(mImage).execute(day.getImagePath());
    }

    private boolean multiple(int position) {
        return position % 2 == 1;
    }
}
