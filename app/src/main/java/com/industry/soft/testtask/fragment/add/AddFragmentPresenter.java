package com.industry.soft.testtask.fragment.add;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.industry.soft.testtask.App;
import com.industry.soft.testtask.R;
import com.industry.soft.testtask.cicerone.AppRouter;
import com.industry.soft.testtask.cicerone.Screens;
import com.industry.soft.testtask.model.OpenWeather;
import com.industry.soft.testtask.api.openweather.OpenWeatherAPI;
import com.industry.soft.testtask.model.Hit;
import com.industry.soft.testtask.model.Pixabay;
import com.industry.soft.testtask.api.pixabay.PixabayAPI;
import com.industry.soft.testtask.realm.RealmManager;
import com.industry.soft.testtask.utils.ConnectionManager;
import com.industry.soft.testtask.model.Day;
import com.industry.soft.testtask.utils.DownloadImageFromUrl;
import com.industry.soft.testtask.utils.SavedImageToFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class AddFragmentPresenter extends MvpPresenter<AddFragmentView> {

    public static final int CAMERA_REQUEST = 10;
    public static final int GALLERY_REQUEST = 20;

    private Activity mActivity;
    private AppRouter mRouter;
    private RealmManager mRealmManager;
    private File mOutput;
    private ConnectionManager mConnectivityManager;
    private Day mDay;
    private Bitmap mDownloadedImage;

    public AddFragmentPresenter(Activity activity, AppRouter router, RealmManager realmManager) {
        mActivity = activity;
        mRouter = router;
        mRealmManager = realmManager;
        mDay = new Day();
        mConnectivityManager = new ConnectionManager(activity, router);
    }

    public void loadWeather(final OpenWeatherAPI api) {
        mConnectivityManager.checkConnection(new ConnectionManager.Connection() {
            @Override
            public void connected() {
                Call<OpenWeather> response = api.callByAreaId("978539e18a215484b0146ed80b932145", "710734", "metric");
                response.enqueue(new Callback<OpenWeather>() {
                    @Override
                    public void onResponse(Call<OpenWeather> call, Response<OpenWeather> response) {
                        getViewState().lockUI();
                        mDay.setDate(getCurrentDateTime());
                        if (200 == response.code()) {
                            mDay.setLocation("Chernihiv");
                            mDay.setDegree(response.body().getMain().getTemp() + mActivity.getString(R.string.celsius));
                            getViewState().setInfo(mActivity.getString(R.string.info_full, mDay.getDate(), mDay.getLocation(), mDay.getDegree()));
                        }

                        getViewState().unlockUI();
                    }

                    @Override
                    public void onFailure(Call<OpenWeather> call, Throwable t) {
                        mRouter.showError(t.getMessage());
                    }
                });
            }

            @Override
            public void notConnected() {
                getViewState().setInfo(mDay.getDate());
            }
        });
    }

    public void loadImage(final PixabayAPI api) {
        mConnectivityManager.checkConnection(new ConnectionManager.Connection() {
            @Override
            public void connected() {
                Call<Pixabay> response = api.list("8255225-f86270e0f56a271122ac11199", "winter", true);
                response.enqueue(new Callback<Pixabay>() {
                    @Override
                    public void onResponse(@NonNull Call<Pixabay> call, @NonNull Response<Pixabay> pixabay) {
                        getViewState().lockUI();
                        if (200 == pixabay.code()) {
                            Bitmap downloadedBitmap = getBitmapFromUrl(getRandomHit(pixabay
                                    .body()
                                    .getHits()).getPreviewURL());
                            mDownloadedImage = downloadedBitmap;
                            mDay.setImagePath(createFile().getAbsolutePath());
                            new SavedImageToFile(mDay.getImagePath()).execute(mDownloadedImage);
                            getViewState().setImage(downloadedBitmap);
                        } else if (429 == pixabay.code()) {
                            mRouter.showWarning(mActivity.getString(R.string.many_request));
                        }
                        getViewState().unlockUI();
                    }

                    @Override
                    public void onFailure(@NonNull Call<Pixabay> call, @NonNull Throwable t) {
                        mRouter.showError(t.getMessage());
                    }
                });
            }

            @Override
            public void notConnected() {
            }
        });
    }

    private String getCurrentDateTime() {
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat(
                mActivity.getResources().getString(R.string.data_and_time_format),
                Locale.getDefault());
        return dateTimeFormat.format(new Date());
    }

    private Bitmap getBitmapFromUrl(String url) {
        Bitmap bitmap = null;
        try {
            bitmap = new DownloadImageFromUrl().execute(url).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    private Hit getRandomHit(List<Hit> hits) {
        int rand = new Random().nextInt(hits.size());
        return hits.get(rand);
    }

    public Uri createUri() {
        return Uri.fromFile(createFile());
    }

    private File createFile() {
        File dir = mActivity.getExternalFilesDir("");
        mOutput = new File(dir, mDay.getDate() + mActivity.getResources().getString(R.string.jpg));
        return mOutput;
    }

    public void cameraResult() {
        mDay.setImagePath(mOutput.getAbsolutePath());
        mDownloadedImage = createBitmap(mOutput);
        getViewState().setImage(mDownloadedImage);
    }

    private Bitmap createBitmap(File file) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(file.getAbsolutePath(), options);
    }

    public void galleryResult(Intent data) {
        try {
            // TODO: Don't know yet what to do with this ("Out of memory error").
            mDownloadedImage = MediaStore.Images.Media.getBitmap(mActivity.getContentResolver(), data.getData());
            mDay.setImagePath(createFile().getAbsolutePath());
            new SavedImageToFile(mDay.getImagePath()).execute(mDownloadedImage);
            getViewState().setImage(mDownloadedImage);
        } catch (OutOfMemoryError e) {
            App.log(mActivity.getResources().getString(R.string.out_of_memory_error));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save(String title, String description) {
        getViewState().showProgress();
        mDay.setTitle(title);
        mDay.setDescription(description);
        if (mDay.getLocation() == null) {
            mDay.setLocation(mActivity.getString(R.string.empty_space));
        }
        if (mDay.getDegree() == null) {
            mDay.setDegree(mActivity.getString(R.string.empty_space));
        }
        mRealmManager.save(mDay);
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                clearData();
                getViewState().hideProgress();
            }
        }, 1200);
    }

    private void clearData() {
        mDay = new Day();
        mDay.setDate(getCurrentDateTime());
        mOutput = createFile();
        mDownloadedImage = null;
        mRouter.showSuccess(mActivity.getString(R.string.day_saved));
        getViewState().clearViews();
        getViewState().setImage(ContextCompat.getDrawable(mActivity, R.drawable.picture));
    }

    public void onBackPressed() {
        mRouter.finishChain();
    }

    public void showDaysList() {
        mRouter.navigateTo(Screens.LIST_FRAGMENT);
    }

    public void hideKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }
}
