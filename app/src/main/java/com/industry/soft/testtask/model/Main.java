package com.industry.soft.testtask.model;

import com.google.gson.annotations.SerializedName;

/**
 * {@link com.industry.soft.testtask.api.openweather.OpenWeatherAPI} model.
 */
public class Main {

    @SerializedName("temp")
    private String mTemp;

    public String getTemp() {
        return mTemp;
    }

    @Override
    public String toString() {
        return "temp: " + mTemp;
    }
}
