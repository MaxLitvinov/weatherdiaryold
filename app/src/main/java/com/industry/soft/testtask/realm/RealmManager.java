package com.industry.soft.testtask.realm;

import com.industry.soft.testtask.fragment.list.adapter.DayAdapter;
import com.industry.soft.testtask.model.Day;

import java.util.List;

import io.realm.Realm;

public class RealmManager {

    private Realm mRealm;

    public RealmManager() {
        mRealm = Realm.getDefaultInstance();
    }

    public void save(final Day day) {
        checkState();
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insert(day);
            }
        });
    }

    /**
     * Load all saved days to adapter.
     *
     * @param adapter Adapter for loading saved days.
     */
    public void load(DayAdapter adapter) {
        load(adapter, 0, 20);
    }

    /**
     * Use when user scrolls RecyclerView.
     *
     * @param adapter   Adapter for loading saved days.
     * @param fromIndex Start day index.
     * @param toIndex   End day index.
     */
    public void load(final DayAdapter adapter, final int fromIndex, final int toIndex) {
        checkState();
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                List<Day> days = null;
                try {
                    days = realm.where(Day.class).findAll();
                    if (days.size() > toIndex) {
                        days = days.subList(fromIndex, toIndex);
                    } else {
                        days = days.subList(fromIndex, days.size());
                    }
                } finally {
                    if (days != null && !days.isEmpty()) {
                        adapter.load(days);
                    }
                }
            }
        });
    }

    /**
     * Check Realm state.
     * If it is closed then get default instance of Realm.
     */
    private void checkState() {
        if (mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }
    }

    public void closeRealm() {
        if (mRealm != null) {
            if (!mRealm.isClosed()) {
                mRealm.close();
            }
        }
    }
}
