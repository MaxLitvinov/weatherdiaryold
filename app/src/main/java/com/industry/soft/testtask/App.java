package com.industry.soft.testtask;

import android.app.Application;
import android.content.Context;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;

import com.industry.soft.testtask.dagger.AppComponent;
import com.industry.soft.testtask.dagger.AppModule;
import com.industry.soft.testtask.dagger.ApiModule;
import com.industry.soft.testtask.dagger.DaggerAppComponent;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class App extends Application {

    private AppComponent mAppComponent;

    public static AppComponent getAppComponent(Context context) {
        App app = (App) context.getApplicationContext();
        return app.mAppComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true); // For VectorDrawable in selectors.
        initRealm();
        initDagger();
    }

    private void initRealm() {
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    private void initDagger() {
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule())
                .apiModule(new ApiModule())
                .build();
    }

    /**
     * Temp method for logging. Remove in release.
     */
    public static void log(String message) {
        final String tag = "APP_TAG";
        int length = 4000;
        if (message.length() > length) {
            for (int i = 0; i < message.length(); i += length) {
                if (i + 4000 < message.length()) {
                    Log.d(tag, message.substring(i, i + length));
                } else {
                    Log.d(tag, message.substring(i, message.length()));
                }
            }
        } else {
            Log.d("TAG", message);
        }
    }
}
