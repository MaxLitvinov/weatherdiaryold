package com.industry.soft.testtask.dagger;

import com.industry.soft.testtask.api.openweather.OpenWeatherAPI;
import com.industry.soft.testtask.api.pixabay.PixabayAPI;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    @Provides
    @Singleton
    @Named("pixabay")
    PixabayAPI providePixabay() {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://pixabay.com/api/")
                .client(new OkHttpClient.Builder().build())
                .build()
                .create(PixabayAPI.class);
    }

    @Provides
    @Singleton
    @Named("openweather")
    OpenWeatherAPI provideOpenWeather() {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://api.openweathermap.org/")
                .client(new OkHttpClient.Builder().build())
                .build()
                .create(OpenWeatherAPI.class);
    }
}
