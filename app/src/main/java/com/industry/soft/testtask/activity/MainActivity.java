package com.industry.soft.testtask.activity;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.industry.soft.testtask.App;
import com.industry.soft.testtask.R;
import com.industry.soft.testtask.cicerone.AppNavigator;
import com.industry.soft.testtask.cicerone.AppRouter;
import com.industry.soft.testtask.custom.SlideToast;
import com.industry.soft.testtask.fragment.BackListener;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.terrakok.cicerone.NavigatorHolder;

public class MainActivity extends MvpAppCompatActivity implements MainActivityView {

    @Inject
    AppRouter mRouter;
    @Inject
    NavigatorHolder mNavigatorHolder;

    @InjectPresenter
    MainActivityPresenter mPresenter;
    @ProvidePresenter
    MainActivityPresenter providePresenter() {
        return new MainActivityPresenter(mRouter, getSupportFragmentManager());
    }

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;
    @BindView(R.id.st_slide_toast)
    SlideToast mSlideToast;

    private AppNavigator mNavigator;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.getAppComponent(this).inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initNavigationView();

        showStartFragment();
    }

    private void initNavigationView() {
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_add_fragment:
                        onMenuAddClick();
                        break;
                    case R.id.menu_list_fragment:
                        onMenuListClick();
                        break;
                }
                closeDrawer();
                return true;
            }
        });
    }

    private void showStartFragment() {
        mPresenter.loadStartFragment();
    }

    @Override
    protected void onPause() {
        mNavigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        mNavigatorHolder.setNavigator(new AppNavigator(this, getSupportFragmentManager(), R.id.fl_fragment_container));
        mRouter.setSlideToast(mSlideToast);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_fragment_container);
        if (fragment != null && fragment instanceof BackListener && ((BackListener) fragment).onBackPressed()) {
            return;
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onMenuAddClick() {
        mPresenter.showAddFragment();
    }

    @Override
    public void onMenuListClick() {
        mPresenter.showListFragment();
    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(Gravity.START);
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(Gravity.START);
    }
}
