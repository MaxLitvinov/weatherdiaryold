package com.industry.soft.testtask.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * {@link com.industry.soft.testtask.api.pixabay.PixabayAPI} model.
 */
public class Pixabay {

    @SerializedName("totalHits")
    private int mTotalHits;
    @SerializedName("hits")
    private List<Hit> mHits;
    @SerializedName("total")
    private int mTotal;

    public int getTotalHits() {
        return mTotalHits;
    }

    public List<Hit> getHits() {
        return mHits;
    }

    public int getTotal() {
        return mTotal;
    }

    @Override
    public String toString() {
        return "totalHits: " + mTotalHits + '\n' +
                "     hits: " + mHits + '\n' +
                "    total: " + mTotal;
    }
}
