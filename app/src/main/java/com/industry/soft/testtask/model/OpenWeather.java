package com.industry.soft.testtask.model;

import com.google.gson.annotations.SerializedName;

/**
 * {@link com.industry.soft.testtask.api.openweather.OpenWeatherAPI} model.
 */
public class OpenWeather {

    @SerializedName("main")
    private Main mMain;

    public Main getMain() {
        return mMain;
    }

    @Override
    public String toString() {
        return "main: " + mMain;
    }
}
