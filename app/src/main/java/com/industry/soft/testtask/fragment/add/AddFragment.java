package com.industry.soft.testtask.fragment.add;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.industry.soft.testtask.App;
import com.industry.soft.testtask.R;
import com.industry.soft.testtask.activity.MainActivity;
import com.industry.soft.testtask.api.openweather.OpenWeatherAPI;
import com.industry.soft.testtask.api.pixabay.PixabayAPI;
import com.industry.soft.testtask.cicerone.AppRouter;
import com.industry.soft.testtask.fragment.BackListener;
import com.industry.soft.testtask.fragment.BaseFragment;
import com.industry.soft.testtask.fragment.OnSwipeTouchListener;
import com.industry.soft.testtask.realm.RealmManager;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;

import static com.industry.soft.testtask.fragment.add.AddFragmentPresenter.CAMERA_REQUEST;
import static com.industry.soft.testtask.fragment.add.AddFragmentPresenter.GALLERY_REQUEST;

public class AddFragment extends BaseFragment implements BackListener, AddFragmentView {

    @Inject
    AppRouter mRouter;
    @Inject
    RealmManager mRealmManager;

    @Inject
    @Named("pixabay")
    PixabayAPI mPixabay;
    @Inject
    @Named("openweather")
    OpenWeatherAPI mOpenWeather;

    @InjectPresenter
    AddFragmentPresenter mPresenter;

    @ProvidePresenter
    AddFragmentPresenter providePresenter() {
        return new AddFragmentPresenter(getActivity(), mRouter, mRealmManager);
    }

    @BindView(R.id.iv_toolbar_menu)
    ImageView mToolbarMenu;
    @BindView(R.id.tv_toolbar_title)
    TextView mToolbarTitle;

    @BindView(R.id.cl_container)
    ConstraintLayout mContainer;
    @BindView(R.id.et_title)
    EditText mTitle;
    @BindView(R.id.tv_info)
    TextView mInfo;
    @BindView(R.id.iv_image)
    ImageView mImage;
    @BindView(R.id.pb_progress)
    ProgressBar mProgress;
    @BindView(R.id.et_description)
    EditText mDescription;
    @BindView(R.id.iv_download)
    ImageView mDownload;
    @BindView(R.id.iv_shutter)
    ImageView mShutter;
    @BindView(R.id.iv_gallery)
    ImageView mGallery;
    @BindView(R.id.pb_main_progress)
    ProgressBar mMainProgress;

    public static AddFragment newInstance() {
        return new AddFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        App.getAppComponent(getContext()).inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_add;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void initView(View rootView) {
        mToolbarTitle.setText(getString(R.string.title_add));
        mImage.buildDrawingCache(true);
        // TODO: Don't know how to do override performClick().
        mContainer.setOnTouchListener(swipeListener());
    }

    private View.OnTouchListener swipeListener() {
        return new OnSwipeTouchListener(getContext()) {
            @Override
            public void onSwipeLeft() {
                showListFragment();
            }
        };
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.loadWeather(mOpenWeather);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_REQUEST) {
                mPresenter.cameraResult();
            } else if (requestCode == GALLERY_REQUEST) {
                mPresenter.galleryResult(data);
            }
        }
    }

    @OnClick(R.id.iv_toolbar_menu)
    public void menuClick() {
        ((MainActivity) getActivity()).openDrawer();
    }

    @OnEditorAction(R.id.et_description)
    protected boolean descriptionDone(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            mPresenter.hideKeyboard(mDescription);
            mDescription.clearFocus();
            mInfo.requestFocus();
            return true;
        }
        return false;
    }

    @OnClick(R.id.iv_download)
    public void downloadPhoto() {
        mPresenter.loadImage(mPixabay);
    }

    @OnClick(R.id.iv_shutter)
    public void makePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mPresenter.createUri());
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    @OnClick(R.id.iv_gallery)
    public void takeFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, GALLERY_REQUEST);
    }

    @OnClick(R.id.iv_add)
    public void add() {
        mPresenter.save(mTitle.getText().toString(), mDescription.getText().toString());
    }

    @Override
    public boolean onBackPressed() {
        mPresenter.onBackPressed();
        return true;
    }

    @Override
    public void setImage(Bitmap image) {
        mImage.setImageBitmap(image);
    }

    @Override
    public void setImage(Uri uri) {
        mImage.setImageURI(uri);
    }

    @Override
    public void setImage(Drawable drawable) {
        mImage.setImageDrawable(drawable);
    }

    @Override
    public void setInfo(String info) {
        mInfo.setText(info);
    }

    @Override
    public void clearViews() {
        mInfo.setText(getString(R.string.empty_space));
        mTitle.setText(getString(R.string.empty_space));
        mDescription.setText(getString(R.string.empty_space));
        mImage.destroyDrawingCache();
        mImage.setImageBitmap(null);
        mImage.setBackgroundResource(0);
        mImage.setImageResource(0);
        mPresenter.loadWeather(mOpenWeather);
    }

    @Override
    public void showListFragment() {
        mPresenter.showDaysList();
    }

    @Override
    public void lockUI() {
        lock(mDownload, mShutter, mGallery);
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void unlockUI() {
        unlock(mDownload, mShutter, mGallery);
        mProgress.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        mMainProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mMainProgress.setVisibility(View.GONE);
    }

    private void lock(View... view) {
        for (View v : view) {
            v.setEnabled(false);
        }
    }

    private void unlock(View... view) {
        for (View v : view) {
            v.setEnabled(true);
        }
    }
}
