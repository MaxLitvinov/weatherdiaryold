package com.industry.soft.testtask.fragment.list.adapter;

public interface OnLoadMoreListener {

    void onLoadMore();
}
