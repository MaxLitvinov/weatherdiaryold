package com.industry.soft.testtask.fragment.list.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.industry.soft.testtask.R;
import com.industry.soft.testtask.model.Day;

import java.util.ArrayList;
import java.util.List;

public class DayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final static int VIEW_SHOW_ITEM = 1;
    private final static int VIEW_PROGRESS = 2;

    private List<Day> mDays;

    private int mVisibleThreshold = 5;
    private int mLastVisibleItem;
    private int mTotalItemCount;
    private boolean mIsLoading;

    private OnLoadMoreListener mOnLoadMoreListener;

    public DayAdapter() {
        mDays = new ArrayList<>();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    mTotalItemCount = linearLayoutManager.getItemCount();
                    mLastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!mIsLoading && mTotalItemCount <= (mLastVisibleItem + mVisibleThreshold)) {
                        // TODO: End has been reached. Do something.
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();
                        }
                        mIsLoading = true;
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_SHOW_ITEM) {
            View showItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_days, parent, false);
            return new DayHolder(showItemView);
        } else {
            View progressView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_progress, parent, false);
            return new ProgressHolder(progressView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mDays.get(position) != null ? VIEW_SHOW_ITEM : VIEW_PROGRESS;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DayHolder) {
            ((DayHolder) holder).bind(mDays.get(position));
        } else if (holder instanceof ProgressHolder){
            ((ProgressHolder) holder).bind();
        }
    }

    public void setLoaded() {
        mIsLoading = false;
    }

    public List<Day> getDays() {
        return mDays;
    }

    @Override
    public int getItemCount() {
        return mDays.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener listener) {
        mOnLoadMoreListener = listener;
    }

    public void load(List<Day> days) {
        mDays.addAll(days);
        notifyDataSetChanged();
    }
}
