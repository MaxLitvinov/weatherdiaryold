package com.industry.soft.testtask.dagger;

import com.industry.soft.testtask.activity.MainActivity;
import com.industry.soft.testtask.fragment.add.AddFragment;
import com.industry.soft.testtask.fragment.list.ListFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, ApiModule.class})
public interface AppComponent {

    void inject(MainActivity activity);
    void inject(AddFragment fragment);
    void inject(ListFragment fragment);
}
