package com.industry.soft.testtask.dagger;

import com.industry.soft.testtask.cicerone.AppRouter;
import com.industry.soft.testtask.realm.RealmManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;

@Module
public class AppModule {

    private Cicerone<AppRouter> mCicerone;

    public AppModule() {
        mCicerone = Cicerone.create(new AppRouter());
    }

    @Singleton
    @Provides
    AppRouter provideRouter() {
        return mCicerone.getRouter();
    }

    @Singleton
    @Provides
    NavigatorHolder provideNavigatorHolder() {
        return mCicerone.getNavigatorHolder();
    }

    @Singleton
    @Provides
    RealmManager provideRealm() {
        return new RealmManager();
    }
}
