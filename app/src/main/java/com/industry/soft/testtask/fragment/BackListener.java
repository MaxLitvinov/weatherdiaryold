package com.industry.soft.testtask.fragment;

/**
 * Device back button listener.
 */
public interface BackListener {

    boolean onBackPressed();
}
