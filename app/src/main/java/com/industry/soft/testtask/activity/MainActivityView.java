package com.industry.soft.testtask.activity;

import com.arellomobile.mvp.MvpView;

public interface MainActivityView extends MvpView {

    void onMenuAddClick();
    void onMenuListClick();
}
