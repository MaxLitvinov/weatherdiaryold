package com.industry.soft.testtask.api.openweather;

import com.industry.soft.testtask.model.OpenWeather;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenWeatherAPI {

    @GET("data/2.5/weather")
    Call<OpenWeather> callByAreaId(@Query("appid") String appId, @Query("id") String areaId, @Query("units") String temperatureMetric);

    @GET("data/2.5/weather")
    Call<ResponseBody> callByLocation(@Query("appid") String appId, @Query("lat") String lat, @Query("lon") String lon, @Query("units") String temperatureMetric);
}
