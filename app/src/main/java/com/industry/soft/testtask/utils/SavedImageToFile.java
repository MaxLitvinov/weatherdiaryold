package com.industry.soft.testtask.utils;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import java.io.FileOutputStream;
import java.io.IOException;

public class SavedImageToFile extends AsyncTask<Bitmap, Void, Void> {

    private String mImagePath;

    /**
     * @param imagePath Path for saving image.
     */
    public SavedImageToFile(String imagePath) {
        mImagePath = imagePath;
    }

    @Override
    protected Void doInBackground(Bitmap... bitmaps) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mImagePath);
            bitmaps[0].compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.flush();
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}