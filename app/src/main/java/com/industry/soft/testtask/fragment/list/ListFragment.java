package com.industry.soft.testtask.fragment.list;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.industry.soft.testtask.App;
import com.industry.soft.testtask.R;
import com.industry.soft.testtask.cicerone.AppRouter;
import com.industry.soft.testtask.fragment.BackListener;
import com.industry.soft.testtask.fragment.BaseFragment;
import com.industry.soft.testtask.fragment.OnSwipeTouchListener;
import com.industry.soft.testtask.fragment.list.adapter.DayAdapter;
import com.industry.soft.testtask.fragment.list.adapter.OnLoadMoreListener;
import com.industry.soft.testtask.realm.RealmManager;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ListFragment extends BaseFragment implements BackListener, ListFragmentView {

    @Inject
    AppRouter mRouter;
    @Inject
    RealmManager mRealmManager;

    @InjectPresenter
    ListFragmentPresenter mPresenter;

    @ProvidePresenter
    ListFragmentPresenter providePresenter() {
        return new ListFragmentPresenter(mRouter, mRealmManager);
    }

    private DayAdapter mAdapter;

    @BindView(R.id.ll_container)
    LinearLayout mContainer;
    @BindView(R.id.iv_toolbar_back)
    ImageView mToolbarMenu;
    @BindView(R.id.tv_toolbar_title)
    TextView mToolbarTitle;

    @BindView(R.id.rv_days)
    RecyclerView mRecyclerView;

    public static ListFragment newInstance() {
        return new ListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        App.getAppComponent(getContext()).inject(this);
        super.onCreate(savedInstanceState);
        mAdapter = new DayAdapter();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_list;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void initView(View rootView) {
        mToolbarTitle.setText(getString(R.string.title_list));
        // TODO: Don't know how to override performClick().
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnTouchListener(recyclerViewSwipeListener());
        mAdapter.setOnLoadMoreListener(adapterLoadMoreListener());
    }

    private OnSwipeTouchListener recyclerViewSwipeListener() {
        return new OnSwipeTouchListener(getContext()) {
            @Override
            public void onSwipeRight() {
                mPresenter.onBackPressed();
            }
        };
    }

    private OnLoadMoreListener adapterLoadMoreListener() {
        return new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mPresenter.loadMoreDays(mAdapter);
                /*mAdapter.getDays().add(null);
                mAdapter.notifyItemInserted(mAdapter.getDays().size() - 1);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.getDays().remove(mAdapter.getDays().size() - 1);
                        int start = mAdapter.getDays().size();
                        int end = start + 20;

                        // TODO: Load days.
                        mRealmManager.load(mAdapter, start, end);
                    }
                }, 500);
                mAdapter.setLoaded();*/
            }
        };
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.loadDays(mAdapter);
    }

    @Override
    public void onDestroy() {
        mRealmManager.closeRealm();
        super.onDestroy();
    }

    @OnClick(R.id.iv_toolbar_back)
    public void toolbarBackButtonClick() {
        onBackPressed();
    }

    @Override
    public boolean onBackPressed() {
        mPresenter.onBackPressed();
        return true;
    }
}
