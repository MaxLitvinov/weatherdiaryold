package com.industry.soft.testtask.model;

import com.google.gson.annotations.SerializedName;

/**
 * {@link com.industry.soft.testtask.api.pixabay.PixabayAPI} model.
 */
public class Hit {

    @SerializedName("largeImageURL")
    private String mLargeImageURL;
    @SerializedName("webformatURL")
    private String mWebformatURL;
    @SerializedName("previewURL")
    private String mPreviewURL;

    public String getLargeImageURL() {
        return mLargeImageURL;
    }

    public String getWebformatURL() {
        return mWebformatURL;
    }

    public String getPreviewURL() {
        return mPreviewURL;
    }

    @Override
    public String toString() {
        return "largeImageURL: " + mLargeImageURL + '\n' +
                " webformatURL: " + mWebformatURL + '\n' +
                "   previewURL: " + mPreviewURL;
    }
}
