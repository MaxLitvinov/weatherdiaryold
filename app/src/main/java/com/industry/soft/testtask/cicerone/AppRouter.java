package com.industry.soft.testtask.cicerone;

import com.industry.soft.testtask.R;
import com.industry.soft.testtask.custom.SlideToast;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

public class AppRouter extends Router {

    private SlideToast mSlideToast;

    @Inject
    public AppRouter() {
    }

    public void setSlideToast(SlideToast toast) {
        mSlideToast = toast;
    }

    public void showError(String message) {
        mSlideToast.makeToast(message, R.color.slide_toast_error).show();
    }

    public void showSuccess(String message) {
        mSlideToast.makeToast(message, R.color.slide_toast_ok).show();
    }

    public void showWarning(String message) {
        mSlideToast.makeToast(message, R.color.slide_toast_warning).show();
    }
}
