package com.industry.soft.testtask.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.URL;

public class DownloadImageFromUrl extends AsyncTask<String, Void, Bitmap> {

    protected Bitmap doInBackground(String... urls) {
        Bitmap bitmap = null;
        try {
            InputStream in = new URL(urls[0]).openStream();
            bitmap = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}