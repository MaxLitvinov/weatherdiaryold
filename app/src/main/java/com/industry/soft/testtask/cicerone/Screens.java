package com.industry.soft.testtask.cicerone;

/**
 * Note that fields must be named like names of fragments
 * <br>because of {@link com.industry.soft.testtask.activity.MainActivityPresenter},
 * <br>which can get current screen (Fragment) by its name.
 */
public class Screens {

    public static final String ADD_FRAGMENT = "AddFragment";
    public static final String LIST_FRAGMENT = "ListFragment";
}