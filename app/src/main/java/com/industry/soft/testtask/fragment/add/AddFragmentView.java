package com.industry.soft.testtask.fragment.add;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import com.arellomobile.mvp.MvpView;

public interface AddFragmentView extends MvpView {

    /**
     * Use when picking up image from camera.
     */
    void setImage(Bitmap image);
    /**
     * Use when picking up image from gallery.
     */
    void setImage(Uri uri);
    void setImage(Drawable drawable);
    void setInfo(String info);
    /**
     * Reset all views to default state.
     */
    void clearViews();
    void showListFragment();
    /**
     * Lock buttons and show ProgressBar.
     * Use while image is loading.
     */
    void lockUI();
    /**
     * Unlock buttons and hide ProgressBar.
     * Use when image was loaded.
     */
    void unlockUI();
    void showProgress();
    void hideProgress();
}
