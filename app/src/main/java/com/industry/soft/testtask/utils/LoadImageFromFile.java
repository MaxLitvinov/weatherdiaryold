package com.industry.soft.testtask.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;

import com.industry.soft.testtask.custom.RoundedImageView;

import java.io.File;
import java.lang.ref.WeakReference;

public class LoadImageFromFile extends AsyncTask<String, Void, Bitmap> {

    private WeakReference<RoundedImageView> mImage;

    /**
     * @param image View where will be loaded image.
     */
    public LoadImageFromFile(RoundedImageView image) {
        mImage = new WeakReference<>(image);
    }

    protected Bitmap doInBackground(String... paths) {
        if (paths[0] == null) {
            return null;
        }
        File file = new File(paths[0]);
        Bitmap bitmap = null;
        if (file.exists()) {
            bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(paths[0]), 100, 100);
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if (bitmap != null) {
            mImage.get().setImageBitmap(bitmap);
        }
    }
}