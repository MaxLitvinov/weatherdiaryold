package com.industry.soft.testtask.api.pixabay;

import com.industry.soft.testtask.model.Pixabay;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PixabayAPI {

    @GET(".")
    Call<Pixabay> list(@Query("key") String key, @Query("q") String query, @Query("pretty") boolean pretty);
}
