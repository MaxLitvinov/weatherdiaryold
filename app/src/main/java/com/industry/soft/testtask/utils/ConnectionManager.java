package com.industry.soft.testtask.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.industry.soft.testtask.R;

import ru.terrakok.cicerone.Router;

public class ConnectionManager {

    private Context mContext;
    private Router mRouter;

    public ConnectionManager(Context context, Router router) {
        mContext = context;
        mRouter = router;
    }

    public void checkConnection(Connection connection) {
        if (isAnyConnect(mContext)) {
            connection.connected();
        } else {
            mRouter.showSystemMessage(mContext.getString(R.string.no_internet));
            connection.notConnected();
        }
    }

    private boolean isAnyConnect(Context context) {
        if (isConnected(context)) {
            if (isConnectedWifi(context) || isConnectedMobile(context)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if there is any connectivity.
     */
    private boolean isConnected(Context context){
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    /**
     * Check if there is any connectivity to a Wifi network.
     */
    private boolean isConnectedWifi(Context context){
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    /**
     * Check if there is any connectivity to a mobile network.
     */
    private boolean isConnectedMobile(Context context){
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    private NetworkInfo getNetworkInfo(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public interface Connection {
        void connected();
        void notConnected();
    }
}
